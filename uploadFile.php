<!DOCTYPE html>
<html>
<body>

<?php
session_start();
// Get the filename and make sure it is valid
$filename = basename($_FILES['fileUpload']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

// Get the username and make sure it is valid
$username = $_SESSION['username'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}

$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);

if( move_uploaded_file($_FILES['fileUpload']['tmp_name'], $full_path) ){
	header("Location: home_user.php");
	exit;
}else{
	header("Location: home_user.php");
	exit;
}
echo "done";
?>

</body>
</html>
