<!DOCTYPE html>
<html>
<body>

<?php

//read file of users line by line
$h = fopen("users.txt", "r");  //r is read
$username = $_GET["user"];

//iterate over all users in txt file
$line =1;
while(!feof($h)){
    $string = fgets($h);

    //compare attempted username to usernames in list
    if($username == $string){
        session_start();
        $_SESSION['username'] = $username;
        header('Location: home_user.php'); //redirects to other page
        exit;
    }
    else{
        $line++;  //keep iterating through
    }

}
//if nothing was found friendly message about making a new account

echo "Sorry guys, no account found.";
//echo "<br>";
echo "Please make a new account at the bottom of the login page.";


?>
<form name="back" action="module2_files.html" name="Go back">
  <p>
    <button type="submit">Back</button>
  </p> </form>
</body>

</html>
